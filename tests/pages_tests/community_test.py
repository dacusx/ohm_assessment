
from __future__ import absolute_import
from app_main import app
from tests import OhmTestCase


class CommunityTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            response = c.get('/community')
            assert b"Community" in response.data
            assert b"Name" in response.data
            assert b"Tier" in response.data
            assert b"Points" in response.data
            assert b"Phone" in response.data
