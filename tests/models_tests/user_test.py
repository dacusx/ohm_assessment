from __future__ import absolute_import
from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_single(self):
        self.chuck.set_display_name('TEST Name')
        assert self.chuck.full_name() == 'TEST Name'
        self.chuck.set_display_name('Chuck Norris')
        assert self.chuck.full_name() == 'Chuck Norris'
        assert self.chuck.short_name() == 'Chuck Norris'
        assert self.justin.get_points_and_dollars() == {"points": 0, "dollars": 0}
        assert self.chuck.get_points() == 0
        assert self.chuck.get_email() == 'test@test.com'
        assert self.chuck.get_tier() == 'Carbon'
        assert self.elvis.is_below_tier('Bronze')
        assert self.elvis.is_below_tier('Platinum')
        assert self.chuck.get_id() == 1

    def test_get_most_recent_users(self):
        test = self.object.get_most_recent_users()
        assert isinstance(test, list)
        for row in test:
            assert 'user_id' in row
            assert 'display_name' in row
            assert 'tier' in row
            assert 'point_balance' in row
            assert 'phones' in row
            assert 'locations' in row
