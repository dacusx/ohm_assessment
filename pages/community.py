from __future__ import absolute_import
from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

@app.route('/community', methods=['GET'])
@app.cache.cached(timeout=300)
def community():

    login_user(User.query.get(1))

    args = {
        'rows': User.get_most_recent_users(),
        'tier_to_color': {
            'Platinum': 'white',
            'Gold': 'gold',
            'Silver': 'silver',
            'Bronze': 'brown',
            'Carbon': 'black',
        }
    }

    return render_template("community.html", **args)

