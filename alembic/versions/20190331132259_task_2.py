"""task 2

Revision ID: 4f5c5d3f7a6c
Revises: 00000000
Create Date: 2019-03-31 13:22:59.519000

"""

# revision identifiers, used by Alembic.
revision = '4f5c5d3f7a6c'
down_revision = '00000000'

from alembic import op


def upgrade():
    op.execute('''UPDATE user
        SET point_balance = 5000
        WHERE user_id = 1
    ''')

    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES
            (2, 'LOCATION', 'USA')
    ''')

    op.execute('''UPDATE user
        SET tier = 'Silver'
        WHERE user_id = 3
    ''')


def downgrade():
    op.execute('''UPDATE user
        SET tier = DEFAULT(tier)
        WHERE user_id = 3
    ''')

    op.execute('''DELETE FROM rel_user
        WHERE user_id = 2
    ''')

    op.execute('''UPDATE user
        SET point_balance = DEFAULT(point_balance)
        WHERE user_id = 1
    ''')
